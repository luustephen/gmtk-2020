﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    public List<GameObject> connectedNodes;

    // Start is called before the first frame update
    void Start()
    {
        foreach (Transform child in transform)
        {
             connectedNodes.Add(child.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public List<GameObject> GetPath()
    {
        return connectedNodes;
    }
}
