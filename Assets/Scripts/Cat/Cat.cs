﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : MonoBehaviour
{
    private List<GameObject> interactables;
    public float waitBetweenMoves = 5;
    public float speed = 1;
    private GameObject goingTo;
    private bool waiting = false;
    private float startWait;

    // Start is called before the first frame update
    void Start()
    {
        interactables = new List<GameObject>();
        GameObject[] temp = FindObjectsOfType<GameObject>();
        for(int i = 0; i < temp.Length; i++)
        {
            if (temp[i].tag == "Interactable" || temp[i].tag == "CatPos")
                interactables.Add(temp[i]);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if(Time.time - waitBetweenMoves > startWait)
        {
            waiting = false;
        }
        if (!waiting && goingTo == null)
        {
            goingTo = interactables[Random.Range(0, interactables.Count)];
        }
        else if(!waiting && goingTo)
        {
            transform.Translate((goingTo.transform.position - transform.position)/100 * speed);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Interactable")
        {
            goingTo = null;
            waiting = true;
            startWait = Time.time;
        }
    }
}
