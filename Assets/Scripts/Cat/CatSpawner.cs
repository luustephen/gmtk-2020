﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatSpawner : MonoBehaviour
{

    private GameObject[] pathNodes;
    private GameObject catPath;
    public GameObject catPrefab;
    public float timeBetweenCatSpawns = 10;
    public int numMainNodes = 100;

    // Start is called before the first frame update
    void Start()
    {
        catPath = GameObject.Find("Cat Path");
        pathNodes = new GameObject[numMainNodes];

        int i = 0;
        foreach (Transform child in catPath.transform)
        {
            if (child.tag == "Node")
            {
                pathNodes[i] = child.gameObject;
                i++;
            }
        }

        InvokeRepeating("SpawnCat", timeBetweenCatSpawns, timeBetweenCatSpawns);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SpawnCat()
    {
        GameObject cat = Instantiate(catPrefab);
        int temp = Random.Range(0, pathNodes.Length);
        cat.transform.position = pathNodes[temp].transform.position;
        cat.GetComponent<CatPathing>().nodeStart = pathNodes[temp];
        cat.GetComponent<CatPathing>().speed = Random.Range(.3f, 2);
        cat.GetComponent<CatPathing>().waitBetweenMoves = Random.Range(2,8);
    }
}
