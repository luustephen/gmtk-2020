﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatPathing : MonoBehaviour
{
    private GameObject[] pathNodes;
    private GameObject catPath;
    public int numMainNodes = 100;
    public float waitBetweenMoves = 4;
    public float speed = 1;
    private float startWait;
    private bool waiting = false;
    private GameObject goingTo;
    private GameObject nodeAt;
    private GameObject cameFrom;
    public GameObject nodeStart;
    private float actualWait = 4;
    private Animator animController;

    // Start is called before the first frame update
    void Start()
    {
        animController = GetComponent<Animator>();
        actualWait = waitBetweenMoves;
        catPath = GameObject.Find("Cat Path");
        nodeAt = nodeStart;
        pathNodes = new GameObject[numMainNodes];
        int i = 0;
        foreach (Transform child in catPath.transform)
        {
            if (child.tag == "Node")
            {
                pathNodes[i] = child.gameObject;
                i++;
            }
        }

        for (int k = 0; k < pathNodes.Length; k++)
        {
            if (k > 0)
                pathNodes[k].GetComponent<Node>().connectedNodes.Add(pathNodes[k - 1]);
            if (k < pathNodes.Length-1)
                pathNodes[k].GetComponent<Node>().connectedNodes.Add(pathNodes[k + 1]);
        }

        /*for(int k = 0; k < pathNodes.Length; k++)
        {
            print(pathNodes[k]);
        }*/

    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - actualWait > startWait)
        {
            waiting = false;
            actualWait = waitBetweenMoves;
        }
        if (!waiting && goingTo == null && nodeAt.tag == "Node")
        {
            List<GameObject> connectNodes = nodeAt.GetComponent<Node>().connectedNodes;
            goingTo = connectNodes[Random.Range(0, connectNodes.Count)];
        }
        else if (!waiting && goingTo == null && nodeAt.tag != "Node")
        {
            goingTo = cameFrom;
        }
        else if (!waiting && goingTo)
        {
            transform.Translate((goingTo.transform.position - transform.position) / 100 * speed);

            float diffY = goingTo.transform.position.y - transform.position.y;
            float diffX = goingTo.transform.position.x - transform.position.x;
            if (Mathf.Abs(diffY) >= Mathf.Abs(diffX) && (Mathf.Abs(diffY) > 0.5f || Mathf.Abs(diffX) >0.5f))
            {
                if(diffY >= 0)
                    animController.SetInteger("Direction", 1); //Up
                else
                    animController.SetInteger("Direction", 3); //Down
            }
            else
            {
                if (diffX >= 0)
                    animController.SetInteger("Direction", 2); //Right
                else
                    animController.SetInteger("Direction", 4); //Left
            }
        }

        if (waiting && animController.GetInteger("Direction") > 0)
        {
            animController.SetInteger("Direction" , -animController.GetInteger("Direction"));
        }
    }

    public GameObject[] GetPath()
    {
        return pathNodes;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == goingTo)
        {
            cameFrom = nodeAt;
            nodeAt = goingTo;
            goingTo = null;
            waiting = true;
            startWait = Time.time;
        }

        if (collision.gameObject.name == "Food Bowl")
            actualWait *= 2;
    }
}
