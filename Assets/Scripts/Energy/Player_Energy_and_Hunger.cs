﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Energy_and_Hunger : MonoBehaviour
{
    [Header("Initial Energy")]
    [SerializeField] float energy = 100f;
    [SerializeField] float maxEnergy = 100f;
    // [SerializeField] float hunger = 100f;

    [Header("Energy Depletion per Second")]
    [SerializeField] float walking = 1f;

    [Header("Energy Restoration per Second")]
    [SerializeField] float idle = 2f;
    [SerializeField] float resting = 5f;

    [Header("Bed")]
    [SerializeField] Bed bed;

    private Movement playerMovement;
    private GameObject player;
    private bool ranOut = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerMovement = player.GetComponent<Movement>();
    }

    // Update is called once per frame
    void Update()
    {
        if(ranOut && energy >= 10)
        {
            playerMovement.UnfreezeMovement();
        }
        if(GetComponent<Movement>().PlayerMoving())
        {
            AddEnergyDeltaTime(-walking);
        } 
        else if(bed.getUsingBed())
        {
            AddEnergyDeltaTime(resting);
        }
        else {
            AddEnergyDeltaTime(idle);
        }

        //Checks if the energy has reached 0.
        EnergyIsZero();
    }

    // Get Functions
    public float getEnergy(){
        return energy;
    }

    public float getMaxEnergy(){
        return maxEnergy;
    }

    private void AddEnergyDeltaTime(float deltaEnergy){
        float newEnergy = energy + deltaEnergy * Time.deltaTime;
        if (newEnergy <= maxEnergy){
            energy = newEnergy;
        }
    }

    private bool EnergyIsZero(){
        if(energy <= 0){
            energy = 0;
            //FindObjectOfType<GameState>().GameOver();
            playerMovement.FreezeMovement();
            ranOut = true;
            return true;
        }
        return false;
    }

}
