﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnockoverObject : Interactable
{
    private bool knockedOver = false;
    private int numCleaned = 0;

    // Start is called before the first frame update
    void Start()
    {
        gameState = GameObject.Find("GameState").GetComponent<GameState>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Cat" && !knockedOver)
        {
            transform.position = new Vector3(transform.position.x + 1, transform.position.y, 0);
            transform.Rotate(0, 0, -90);
            knockedOver = true;
            sound.Play();
            IncreaseChaos();
        }
    }

    public override bool CheckRequirement()
    {
        if (knockedOver == true)
        {
            transform.position = new Vector3(transform.position.x - 1, transform.position.y, 0);
            knockedOver = false;
            numCleaned++;
            DecreaseChaos();
            transform.Rotate(0, 0, 90);
            return true;
        }
        return false;
    }

    public override int GetFinished()
    {
        return numCleaned;
    }
}
