﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleanupObject : Interactable
{
    private GameObject player;
    private Interact playerInteract;
    private bool knockedOver = false;
    public float cleanTime = 3;
    private float timeCleaned = 0;
    private bool playerClose = false;
    private bool playerEquipped = false;
    private bool playerHolding = false;
    public Sprite brokenSprite;
    private Sprite currentSprite;
    private int numCleaned = 0;

    // Start is called before the first frame update
    void Start()
    {
        currentSprite = GetComponent<SpriteRenderer>().sprite;
        if (slider)
        {
            slider.maxValue = cleanTime;
            slider.value = 0;
        }
        gameState = GameObject.Find("GameState").GetComponent<GameState>();
        player = GameObject.Find("Player");
        playerInteract = player.GetComponent<Interact>();
    }

    // Update is called once per frame
    void Update()
    {
        if (slider && knockedOver)
            slider.gameObject.SetActive(true);
        else
            slider.gameObject.SetActive(false);

        if (slider)
            slider.value = timeCleaned;

        if (Input.GetKey(KeyCode.Mouse0))
            playerHolding = true;
        else
            playerHolding = false;

        //print(knockedOver + " " + playerHolding + " " + playerClose + " " + playerEquipped + " ");
        if (knockedOver && playerHolding && playerClose && playerEquipped) //start cleaning
        {
            timeCleaned += Time.deltaTime;
        }
        if (knockedOver && timeCleaned >=cleanTime) //Finish clean
        {
            knockedOver = false;
            transform.position = new Vector3(transform.position.x-1.1f, transform.position.y, 0);
            GetComponent<SpriteRenderer>().sprite = currentSprite;
            numCleaned++;
            timeCleaned = 0;
            DecreaseChaos();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Cat" && !knockedOver)
        {
            knockedOver = true;
            transform.position = new Vector3(transform.position.x + 1.1f, transform.position.y, 0);
            GetComponent<SpriteRenderer>().sprite = brokenSprite;
            IncreaseChaos();
            sound.Play();
        }
        if(collision.tag == "Player")
        {
            playerClose = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            playerClose = false;
        }
    }

    public override bool CheckRequirement()
    {
        if (knockedOver == true && playerInteract.GetHeldObj() && playerInteract.GetHeldObj().name == "Broom")
        {
            playerEquipped = true;
            return true;
        }
        else if (knockedOver == true && playerInteract.GetHeldObj() && playerInteract.GetHeldObj().name != "Broom")
        {
            playerEquipped = false;
            return true;
        }
        return false;
    }

    public override int GetFinished()
    {
        return numCleaned;
    }
}
