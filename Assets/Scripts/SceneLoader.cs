﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{

    int currentSceneIndex;
    void Start() {
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
    }

    public void RestartScene(){
        Time.timeScale = 1;
        SceneManager.LoadScene(currentSceneIndex);
    }

    public void NextScene(){
        currentSceneIndex++;
        SceneManager.LoadScene(currentSceneIndex);
    }
    
    public void LoadStartMenu(){
        Time.timeScale = 1;
        SceneManager.LoadScene("Start_Screen");
    }

    public void LoadOptionsMenu(){
        Time.timeScale = 1;
        SceneManager.LoadScene("Options_Screen");
    }

    public void QuitGame(){
        Application.Quit();
    }
}
