﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dryer : Interactable
{

    private GameObject player;
    private Interact playerInteract;
    public float dryTime = 5;
    private bool drying = false;
    private int maxLaundry = 1;
    private int numLaundry = 0;
    private float startWash;
    public GameObject dryLaundry;
    private float timeDried = 0; //amount of time laundry has washed for
    private bool catted = false;
    private int numLaundryDried = 0;

    // Start is called before the first frame update
    void Start()
    {
        if (slider)
        {
            slider.maxValue = dryTime;
            slider.value = 0;
        }
        gameState = GameObject.Find("GameState").GetComponent<GameState>();
        player = GameObject.Find("Player");
        playerInteract = player.GetComponent<Interact>();
    }

    // Update is called once per frame
    void Update()
    {
        if ((slider && drying) || catted)
            slider.gameObject.SetActive(true);
        else
            slider.gameObject.SetActive(false);

        if (slider)
            slider.value = timeDried;

        if (drying)
        {
            timeDried += Time.deltaTime;
        }
        if (!catted && !drying && numLaundry == maxLaundry) //Start washing
        {
            startWash = Time.time;
            drying = true;
            sound.Play();
        }
        else if (drying && startWash + timeDried >= startWash + dryTime) //Finish washing
        {
            drying = false;
            dryLaundry.transform.position = new Vector3(transform.position.x, transform.position.y - 1, 0);
            numLaundry = 0;
            numLaundryDried += 3;
            timeDried = 0;
            DecreaseChaos();
            sound.Stop();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Cat" && drying)
        {
            sound.Stop();
            drying = false;
            catted = true;
            IncreaseChaos();
        }
    }

    public override bool CheckRequirement()
    {
        if (playerInteract.GetHeldObj() && playerInteract.GetHeldObj().name == "Clean Laundry")
        {
            numLaundry++;
            playerInteract.RemoveHeldObj();
            return true;
        }
        else if (catted)
        {
            catted = false;
            sound.Play();
        }

        return false;
    }

    public override int GetFinished()
    {
        return numLaundryDried;
    }
}
