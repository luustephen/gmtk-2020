﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Closet : Interactable
{
    private GameObject player;
    private Interact playerInteract;
    public GameObject dirtyLaundry;
    private int numStowed = 0;

    // Start is called before the first frame update
    void Start()
    {
        gameState = GameObject.Find("GameState").GetComponent<GameState>();
        player = GameObject.Find("Player");
        playerInteract = player.GetComponent<Interact>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject newLaundry;
        if (collision.tag == "Cat")
        {
            IncreaseChaos();
            newLaundry = Instantiate(dirtyLaundry);
            newLaundry.transform.position = new Vector3(transform.position.x, transform.position.y - 1, 0);
            newLaundry.name = "Dirty Laundry";
            newLaundry = Instantiate(dirtyLaundry);
            newLaundry.transform.position = new Vector3(transform.position.x + .5f, transform.position.y - 1, 0);
            newLaundry.name = "Dirty Laundry";
            newLaundry = Instantiate(dirtyLaundry);
            newLaundry.transform.position = new Vector3(transform.position.x - .5f, transform.position.y - 1, 0);
            newLaundry.name = "Dirty Laundry";
        }
    }

    public override bool CheckRequirement()
    {
        if (playerInteract.GetHeldObj() && playerInteract.GetHeldObj().name == "Dry Laundry")
        {
            playerInteract.RemoveHeldObj();
            numStowed += 3;
            DecreaseChaos();
            return true;
        }
        return false;
    }

    public override int GetFinished()
    {
        return numStowed;
    }
}
