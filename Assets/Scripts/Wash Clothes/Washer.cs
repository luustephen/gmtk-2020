﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Washer : Interactable
{
    private GameObject player;
    private Interact playerInteract;
    private int maxLaundry = 0;
    private int numLaundry = 0;
    private float startWash;
    public float washTime = 5;
    private float timeWashed = 0; //amount of time laundry has washed for
    private bool washing = false;
    public GameObject cleanLaundry;
    private bool catted = false;
    private int numLaundryWashed = 0;

    // Start is called before the first frame update
    void Start()
    {
        if (slider)
        {
            slider.maxValue = washTime;
            slider.value = 0;
        }
        gameState = GameObject.Find("GameState").GetComponent<GameState>();
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Pickup"))
        {
            if (obj.name == "Dirty Laundry")
            {
                maxLaundry++;
            }
        }
        player = GameObject.Find("Player");
        playerInteract = player.GetComponent<Interact>();
    }

    // Update is called once per frame
    void Update()
    {
        if ((slider && washing) || catted)
            slider.gameObject.SetActive(true);
        else
            slider.gameObject.SetActive(false);

        if (slider)
            slider.value = timeWashed;

        if (washing)
        {
            timeWashed += Time.deltaTime;
        }

        if(!catted && !washing && numLaundry == maxLaundry) //Start washing
        {
            startWash = Time.time;
            washing = true;
            sound.Play();
        }
        else if (washing && startWash + timeWashed >= startWash + washTime) //Finish washing
        {
            washing = false;
            cleanLaundry.transform.position = new Vector3(transform.position.x,transform.position.y-1,0);
            numLaundryWashed += 3;
            numLaundry = 0;
            timeWashed = 0;
            DecreaseChaos();
            sound.Stop();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Cat" && washing)
        {
            sound.Stop();
            washing = false;
            catted = true;
            IncreaseChaos();
        }
    }

    public override bool CheckRequirement()
    {
        if (playerInteract.GetHeldObj() && playerInteract.GetHeldObj().name == "Dirty Laundry")
        {
            numLaundry++;
            playerInteract.RemoveHeldObj();
            return true;
        }
        else if (catted)
        {
            catted = false;
            sound.Play();
        }

        return false;
    }

    public override int GetFinished()
    {
        return numLaundryWashed;
    }
}
