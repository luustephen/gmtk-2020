﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Interactable : MonoBehaviour
{
    protected GameState gameState;
    public float chaosCaused = 5;
    public int scoreAdded = 10;
    public Slider slider;
    public AudioSource sound;
    private AudioSource cat;

    // Start is called before the first frame update
    void Start()
    {
        gameState = GameObject.Find("GameState").GetComponent<GameState>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public abstract bool CheckRequirement();
    public abstract int GetFinished();

    public void IncreaseChaos()
    {
        gameState.increaseChaos(chaosCaused);
        cat = GameObject.Find("Cat").GetComponent<AudioSource>();
        cat.Play();
    }

    public void DecreaseChaos()
    {
        gameState.decreaseChaos(chaosCaused);
        gameState.addScore(scoreAdded);
    }

}
