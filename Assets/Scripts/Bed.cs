﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bed : Interactable
{
    private bool usingBed = false;
    private bool canUseBed = false;
    public GameObject player;

    public BoxCollider2D bedBorder;


    private Player_Energy_and_Hunger playerEnergy;
    private Movement playerMovement;
    // Start is called before the first frame update
    void Start()
    {
        playerEnergy = player.GetComponent<Player_Energy_and_Hunger>();
        playerMovement = player.GetComponent<Movement>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if(Input.GetKeyDown(KeyCode.Mouse0)&&canUseBed){
            Sleep();
        }
    }

    public override bool CheckRequirement(){

        usingBed = !usingBed;
        return true;

        /*
        if(playerEnergy.getEnergy() >= playerEnergy.getMaxEnergy()){
            return false;
        }
        else return true;*/
    }

    public override int GetFinished(){
        return (int) playerEnergy.getEnergy();
    }

    //Freezes player and sets energy to sleeping
    public void Sleep(){
        if (usingBed){
            playerMovement.UnfreezeMovement();
            //So that the player hops out of the bed.
            player.transform.position = new Vector3(transform.position.x, transform.position.y + 1, transform.position.z);
            player.transform.Rotate(0, 0, 90);
            usingBed = false;
            /*Used to deactivate the collider that defines the borders of 
            the bed. This is done so that the player isn't pushed off the 
            bed when sleeping.*/
            bedBorder.enabled = true;
        }
        else {
            bedBorder.enabled = false;
            playerMovement.FreezeMovement();
            player.transform.position = transform.position;
            player.transform.Rotate(0, 0, -90);
            usingBed = true;
        }
        
    }
    public bool getUsingBed(){
        return usingBed;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.CompareTag("Player")){
            canUseBed = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.CompareTag("Player")){
            canUseBed = false;
        }
    }
}
