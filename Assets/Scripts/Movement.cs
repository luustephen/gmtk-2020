﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float speed = 7;                //Floating point variable to store the player's movement speed.

    private Rigidbody2D rb;        //Store a reference to the Rigidbody2D component required to use 2D Physics.

    private bool moving = false;

    private bool freezeMovement = false;

    private Animator animController;

    // Use this for initialization
    void Start()
    {
        animController = GetComponent<Animator>();
        //Get and store a reference to the Rigidbody2D component so that we can access it.
        rb = GetComponent<Rigidbody2D>();
    }

    //FixedUpdate is called at a fixed interval and is independent of frame rate. Put physics code here.
    void FixedUpdate()
    {
        if (tag == "Cat")
        {
            //Store the current horizontal input in the float moveHorizontal.
            float moveHorizontal = Input.GetKey(KeyCode.L) ? 1 : 0;
            if(moveHorizontal == 0)
                moveHorizontal = Input.GetKey(KeyCode.J) ? -1 : 0;
            //Store the current vertical input in the float moveVertical.
            float moveVertical = Input.GetKey(KeyCode.I) ? 1 : 0;
            if(moveVertical == 0)
                moveVertical = Input.GetKey(KeyCode.K) ? -1 : 0;

            //Use the two store floats to create a new Vector2 variable movement.
            Vector2 movement = new Vector2(moveHorizontal * speed * Time.deltaTime * 1.2f, moveVertical * speed * Time.deltaTime);

            //Call the AddForce function of our Rigidbody2D rb2d supplying movement multiplied by speed to move our player.
            rb.MovePosition((Vector2)rb.transform.position + movement);
        }
        else
        {
            if (!freezeMovement){
                Move();
            }
            else if (!moving)
                animController.speed = 0; //Still
        }
    }

    private void Move(){
        //Store the current horizontal input in the float moveHorizontal.
        float moveHorizontal = Input.GetAxis("Horizontal");

        //Store the current vertical input in the float moveVertical.
        float moveVertical = Input.GetAxis("Vertical");

        //Use the two store floats to create a new Vector2 variable movement.
        Vector2 movement = new Vector2(moveHorizontal * speed * Time.deltaTime * 1.2f, moveVertical * speed * Time.deltaTime);

        //Call the AddForce function of our Rigidbody2D rb2d supplying movement multiplied by speed to move our player.
        rb.MovePosition((Vector2)rb.transform.position + movement);

        //Sets the variable moving to true if the player is moving, false if otherwise.
        moving = (moveHorizontal != 0 || moveVertical != 0);

        animController.speed = 1;
        if(moveVertical > 0)
            animController.SetInteger("Direction", 1); //Up
        else if (moveVertical < 0)
            animController.SetInteger("Direction", 3); //Down

        if (moveHorizontal > 0)
            animController.SetInteger("Direction", 2); //Right
        else if (moveHorizontal < 0)
            animController.SetInteger("Direction", 4); //Left

        if (!moving)
            animController.speed = 0; //Still
        else
            animController.speed = 1;
    }

    public void FreezeMovement(){
        moving = false;
        freezeMovement = true;
    }

    public void UnfreezeMovement(){
        freezeMovement = false;
    }

    public bool PlayerMoving(){
        return moving;
    }

}
