﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour
{
    Text scoreText;
    GameState gameState;
    // Start is called before the first frame update
    void Start()
    {
        scoreText = GetComponent<Text>();
        gameState = FindObjectOfType<GameState>();
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Score " + gameState.getScore().ToString();
    }
}
