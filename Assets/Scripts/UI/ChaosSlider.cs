﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChaosSlider : MonoBehaviour
{
    public Slider slider;
    [SerializeField] GameState gameState;
    
    private void Start() {
        slider.maxValue = gameState.getMaxChaos();
        slider.value = gameState.getChaos();
    }

    private void Update() {
        slider.value = gameState.getChaos();
    }
}
