﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergySlider1 : MonoBehaviour
{
    public Slider slider;
    [SerializeField] Player_Energy_and_Hunger energy;
    
    private void Start() {
        slider.maxValue = energy.getMaxEnergy();
        slider.value = energy.getEnergy();

    }

    private void Update() {
        slider.value = energy.getEnergy();
    }
}
