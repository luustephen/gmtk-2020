﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : MonoBehaviour
{
    [SerializeField] int score = 0;
    [SerializeField] float chaos = 0f;
    [SerializeField] float maxChaos = 100f;
    [SerializeField] bool gameOver = false;

    public GameObject gameOverOverlay;

    public GameObject pauseOverlay;

    private bool paused = false;

    private void Start()
    {
        Time.timeScale = 0f;
        InvokeRepeating("increaseChaos",0,5);
        gameOverOverlay.SetActive(false);
        pauseOverlay.SetActive(false);
    }

    private void Update() {
        //Checks if chaos is beyond maxChaos, if so it triggers a GameOver.
        if (chaos >= maxChaos){
            GameOver();
        }

        //Checks if pause was imputed
        if (Input.GetKeyDown(KeyCode.Escape)){
            Pause();
        }

        // Added by Cube
        ShowInstructions();
    }

    // Executed when gameOver conditions are met. Put all required code in here.
    public void GameOver(){
        gameOverOverlay.SetActive(true);
        FindObjectOfType<Player_Energy_and_Hunger>().GetComponent<Movement>().FreezeMovement();
    }

    public void addScore(int deltaScore){
        if(!gameOver){
            score += deltaScore;
        }
    }

    public int getScore(){
        return score;
    }

    public void increaseChaos(float deltaChaos){
        float newChaos = chaos + deltaChaos;
        if(newChaos>=maxChaos){
            newChaos = maxChaos;
        }
        chaos = newChaos;
    }

    public void increaseChaos()
    {
        float newChaos = chaos + 1;
        if (newChaos >= maxChaos)
        {
            newChaos = maxChaos;
        }
        chaos = newChaos;
    }

        public void decreaseChaos(float deltaChaos){
        float newChaos = chaos - deltaChaos;
        if(newChaos<=0){
            newChaos = 0;
        }
        chaos = newChaos;
    }

    public float getChaos(){
        return chaos;
    }

    public float getMaxChaos(){
        return maxChaos;
    }

    public void Pause(){
        if(paused){
            pauseOverlay.SetActive(false);
            paused = false;
            Time.timeScale = 1;
        }
        else {
            pauseOverlay.SetActive(true);
            paused = true;
            Time.timeScale = 0;
        }
        
    }

    // Added by Cube

    bool isShown = true;
    public GameObject instructions;

    public void ShowInstructions()
    {
        if (Input.GetKeyDown(KeyCode.I) && isShown)
        {
            instructions.SetActive(false);
            isShown = false;
            Time.timeScale = 1f;
        }
        else if (Input.GetKeyDown(KeyCode.I) && !isShown)
        {
            instructions.SetActive(true);
            isShown = true;
            Time.timeScale = 0f;
        }
    }
}
