﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour
{

    List<GameObject> interactables;
    private GameObject heldObject;
    private GameObject closestInteractable;
    private GameObject closestPickup;
    private GameObject itemCamera;
    private bool usedAction = false;

    // Start is called before the first frame update
    void Start()
    {
        itemCamera = GameObject.Find("Item Camera");
        interactables = new List<GameObject>();
        heldObject = null;
    }

    // Update is called once per frame
    void Update()
    {
        closestPickup = null;
        closestInteractable = null;
        usedAction = false;
        ClosestInteractable();


        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (heldObject == null && closestPickup != null)
            {
                heldObject = closestPickup;
                closestPickup.transform.position = itemCamera.transform.position;
                closestPickup.transform.Translate(new Vector3(0, 0, 1));
                usedAction = true;
            }

            if (!usedAction && closestInteractable != null && closestInteractable.GetComponent<Interactable>())
            {
                if (closestInteractable.GetComponent<Interactable>().CheckRequirement())
                    usedAction = true;
            }
        }

        if (heldObject && Input.GetKeyDown(KeyCode.Mouse1))
        {
            heldObject.transform.position = transform.position;
            heldObject = null;
            interactables.Remove(heldObject);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Interactable" || collision.tag == "Pickup")
        {
            interactables.Add(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Interactable" || collision.tag == "Pickup")
        {
            interactables.Remove(collision.gameObject);
        }
    }

    private void ClosestInteractable()
    {
        float closestInterDist = 99;
        float closestPickDist = 99;
        float tempDist = 100;
        foreach(GameObject interactable in interactables)
        {
            if (interactable.tag == "Interactable" && (tempDist = Vector3.Distance(transform.position,interactable.GetComponent<Transform>().position)) < closestInterDist)
            {
                closestInterDist = tempDist;
                closestInteractable = interactable;
            }
            tempDist = 100;
            if(interactable.tag == "Pickup" && (tempDist = Vector3.Distance(transform.position, interactable.GetComponent<Transform>().position)) < closestPickDist)
            {
                closestPickDist = tempDist;
                closestPickup = interactable;
            }
        }
    }

    public GameObject GetHeldObj()
    {
        return heldObject;
    }

    public void RemoveHeldObj()
    {
        heldObject.transform.position = new Vector3(transform.position.x, 10, 0);
        heldObject = null;
        if (closestPickup == heldObject)
            closestPickup = null;
        interactables.Remove(heldObject);
    }
}
